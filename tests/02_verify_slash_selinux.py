class TestSlashSELinux(TestBase):
	"""
	Verify that /sys/fs/selinux or /selinux is present
	"""
	class ErrorNoSlashSELinux(ErrorBase):
		def __str__(self):
			return "The directories /sys/fs/selinux and /selinux are missing."
		def fixable(self):
			return True
		def fix(self):
			import os
			return os.mkdir("/selinux")

	@staticmethod
	def test():
		import os
		if not os.access("/sys/fs/selinux",os.F_OK):
			if not os.access("/selinux",os.F_OK):
				return [TestSlashSELinux.ErrorNoSlashSELinux()]
		return []
register_test(TestSlashSELinux)
