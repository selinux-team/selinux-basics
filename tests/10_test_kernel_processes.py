class TestNoKernelT(TestBase):
	"""
	Test for processes running in system_u:system_r:kernel_t
	This type should only be used by kernel processes, which are detected by
	not having any maps.
	"""
	# Not sure if this test actually helps when the init test succeeded.
	# But it doesn't harm either

	class ErrorBadKernelProcesses(ErrorBase):
		def __init__(self, badprocs):
			self.badprocs = badprocs
		def __str__(self):
			return "There were %d processes found running in the kernel domain." \
				% len(self.badprocs)

	@staticmethod
	def test():
		from subprocess import Popen, PIPE

		badprocs = []

		pipe = Popen("getfilecon /proc/[0-9]*", shell=True, stdin=PIPE, stdout=PIPE, close_fds=True, universal_newlines=True)
		pipe.stdin.close()
		for line in pipe.stdout.readlines():
			(dir, context) = line.split()
			components = dir.split("/")
			pid = components[-1]
			if context.find("system_r:kernel_t") >= 0:
				badproc = False
				file = open("/proc/%s/maps" % pid)
				if file.readlines():
					badproc = True
				file.close()
				if badproc:
					badprocs.append(pid)
		pipe.stdout.close()
		if len(badprocs) > 0:
			return [TestNoKernelT.ErrorBadKernelProcesses(badprocs)]
		return []
register_test(TestNoKernelT)
