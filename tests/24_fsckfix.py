class TestFSCKFix(TestBase):
	"""
	Suggest to use FSCKFIX in /etc/default/rcS
	"""
	class ErrorFSCKFix(ErrorBase):
		def __str__(self):
			return "FSCKFIX is not enabled - not serious, but could prevent system from booting..."

	@staticmethod
	def test():
		import os, re
		r = re.compile(r'^\s*FSCKFIX=.*yes')

		if os.access("/etc/default/rcS", os.F_OK):
			fsckfix = False
			f = open("/etc/default/rcS","r")
			for line in f.readlines():
				if r.match(line):
					fsckfix = True
			f.close()
			if not fsckfix:
				return [TestFSCKFix.ErrorFSCKFix()]
		else:
			# Do nothing anymore as /etc/default/rcS is now optional
			pass
		return []
register_test(TestFSCKFix)
