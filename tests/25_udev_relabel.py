class TestRestoreconUdev(TestBase):
	"""
	Check that there is a restorecon call in /etc/init.d/udev
	"""
	class ErrorUdevRelabel(ErrorBase):
		def __str__(self):
			return "udev will create nodes not labeled correctly"

	@staticmethod
	def test():
		import os, re
		r = re.compile(r'(restorecon|relabel)')

		if os.access("/etc/init.d/udev", os.F_OK):
			udevrelabel = False
			f = open("/etc/init.d/udev","r")
			for line in f.readlines():
				if r.search(line):
					udevrelabel = True
			f.close()
			if not udevrelabel:
				return [TestRestoreconUdev.ErrorUdevRelabel()]
		return []
register_test(TestRestoreconUdev)
